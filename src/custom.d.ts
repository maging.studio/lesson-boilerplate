declare module '*.svg' {
  const content: string;
  export default content;
}

declare module '*.png' {
  const content: string;
  export default content;
}

declare module '*.jpg' {
  const content: string;
  export default content;
}

declare module '*.css' {
  const content: string;
  export default content;
}

declare const config: typeof import('../public/lesson.config.json');

/**
 * Доступно только в прод окружении [Leon, AlexDemus]
 */
declare const axiosInstance: import('axios').AxiosInstance;
